const path = require('path');
const appPackage = require('../../package');

// import .env variables
require('dotenv-safe').config({
  path: path.join(__dirname, '../../.env'),
  sample: path.join(__dirname, '../../.env.example'),
});

module.exports = {
  appName: appPackage.name,
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  pg: {
    uri: process.env.DEV_DATABASE_URL,
  },
};
