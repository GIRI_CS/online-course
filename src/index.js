const { port, env } = require('./config/vars');
// eslint-disable-next-line global-require
Promise = require('bluebird'); 
const app = require('./config/express');

// listen to requests
app.listen(port, () => console.info(`API Server started on port ${port} (${env})`)); // eslint-disable-line no-console


// Init sequelize
const db = require('./api/models');
global.sequelize = db;

/**
* Exports express
* @public
*/
module.exports = app;
