const httpStatus = require('http-status');
const moment = require('moment');
const sequelize = require('../models').sequelize;

const Course = sequelize.import('../models/course.model');
const User = sequelize.import('../models/user.model');
const Enrollment = sequelize.import('../models/enrollment.model');
const Attendance = sequelize.import('../models/attendance.model');
const {
    isEmpty,
    toString,
    map,
    pick,
} = require('lodash');

/**
 * Create Course
 * @public
 */
exports.create = async (req, res, next) => {
    try {
        if (isEmpty(toString(req.body.courseenrollmentid)) || isEmpty(toString(req.body.present))) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'One or more params missing' });
        }
        const params = pick(req.body, ['courseenrollmentid', 'present']);
        const enrollment = await Enrollment.findOne({
            where: { id: params.courseenrollmentid }
        });
        if (isEmpty(enrollment)) {
            return res.status(httpStatus.BAD_REQUEST).json({ code: httpStatus.BAD_REQUEST, message: 'Enrollment does not exist in system' });
        }
        let attendance = await Attendance.create(params);
        attendance = pick(attendance.dataValues, ['id', 'markedon']);
        attendance.enrollment = pick(enrollment.dataValues, ['id', 'courseid', 'enrolledOn']);
        return res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'Attendance marked successfully', attendance });
    } catch (error) {
        return next(error);
    }
};

/**
 * Read Course
 * @public
 */
exports.read = async (req, res, next) => {
    try {
        let attendances = await Attendance.findAll({
            where: { courseenrollmentid: req.query.enrollid }
        });
        attendances = map(attendances, attendance => attendance.dataValues);
        if (attendances) {
            return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Attendance fetched successfully', attendances });
        }
        return res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, message: 'Course not found' });
    } catch (error) {
        return next(error);
    }
};

/**
 * List Courses
 * @public
 */
exports.list = async (req, res, next) => {
    try {
        let attendances= await Attendance.findAll();
        attendances = map(attendances, attendance => attendance.dataValues);
        const data = [];
        await Promise.map(attendances, async (attendance) => {
            const enrollment = await Enrollment.findOne({
                where: { id: attendance.courseenrollmentid }
            });
            if(!isEmpty(enrollment)){
                attendance.instructor = pick(enrollment.dataValues, ['id', 'courseid', 'enrolledOn']);
            }
            data.push(attendance);
        });
        return res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Attendance(s) fetched successfully', data });
    } catch (error) {
        return next(error);
    }
};