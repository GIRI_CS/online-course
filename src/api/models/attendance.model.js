'use strict';
module.exports = (sequelize, DataTypes) => {
    // setup User model and its fields.
    const attendance = sequelize.define('attendance', {
        courseenrollmentid: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        present: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        markedon: {
            type: DataTypes.DATE,
            allowNull: true
        }, 
        createdat: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updatedat: {
            type: DataTypes.DATE,
            allowNull: true
        }
    },
    {
        tableName: 'attendance',
    });

    return attendance;
};