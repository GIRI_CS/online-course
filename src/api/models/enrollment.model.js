'use strict';
module.exports = (sequelize, DataTypes) => {
    // setup User model and its fields.
    const enrollments = sequelize.define('enrollments', {
        studentid: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false
        },
        courseid: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        enrolledOn: {
            type: DataTypes.DATE,
            allowNull: true
        }
    },
        {
            tableName: 'enrollments',
        });

    return enrollments;
};