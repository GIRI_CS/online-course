'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
    // setup User model and its fields.
    const users = sequelize.define('users', {
        name: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'student'
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, 
    {
        instanceMethods: {
            validPassword: (password) => {
                return bcrypt.compareSync(password, this.password);
            }
        },
        hooks: {
            beforeCreate: (user) => {
                const salt = bcrypt.genSaltSync();
                user.password = bcrypt.hashSync(user.password, salt);
            }
        },
    }, 
    {
        tableName: 'users',
    });

    users.prototype.validPassword = async function (password) {
        return await bcrypt.compare(password, this.password);
    };

    return users;
};