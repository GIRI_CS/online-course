'use strict';
module.exports = (sequelize, DataTypes) => {
    // setup User model and its fields.
    const courses = sequelize.define('courses', {
        name: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
        instructor: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        createdBy: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    },
    {
        tableName: 'courses',
    });
    courses.associate = function (models) {
        // associations can be defined here
        courses.hasOne(models.users);
    };
    return courses;
};