const express = require('express');
const controller = require('../../controllers/attendance.controller');
const { teacher, student } = require('../../middlewares/auth');
const router = express.Router();

router
    .route('/')
    /**
     * @api {post} api/v1/attendance Create attendance
     * @apiDescription Create attendance
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission teacher
     *
     *
     * @apiSuccess {Object} attendance
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .post(teacher(), controller.create);

router
    .route('/getall')
    /**
    * @api {post} api/v1/attendance Create attendance
    * @apiDescription Create attendance
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission teacher
    *
    *
    * @apiSuccess {Object} attendance
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .get(teacher(), controller.list);

router
    .route('/')
    /**
    * @api {post} api/v1/attendance view attendance
    * @apiDescription view attendance
    * @apiVersion 1.0.0
    * @apiName Create
    * @apiPermission student
    *
    *
    * @apiSuccess {Object} attendance
    *
    * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
    * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
    */
    .get(student(), controller.read);


module.exports = router;
