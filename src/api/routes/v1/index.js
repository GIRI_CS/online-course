const express = require('express');
const userRoutes = require('./user.route');
const courseRoutes = require('./course.route');
const enrollmentRoutes = require('./enrollment.route');
const attendanceRoutes = require('./attendance.route');
const router = express.Router();

/**
 * API Routes
 */
router.use('/api/v1', userRoutes);
router.use('/api/v1/course', courseRoutes);
router.use('/api/v1/enroll', enrollmentRoutes);
router.use('/api/v1/attendance', attendanceRoutes);

module.exports = router;
