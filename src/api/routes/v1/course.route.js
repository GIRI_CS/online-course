const express = require('express');
const controller = require('../../controllers/course.controller');
const { teacher } = require('../../middlewares/auth');
const router = express.Router();

router
    .route('/')
    /**
     * @api {post} api/v1/course Create course
     * @apiDescription Create course
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission teacher
     *
     *
     * @apiSuccess {Object} course
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .post(teacher(), controller.create);

router
    .route('/')
    /**
     * @api {post} api/v1/course Get course
     * @apiDescription Get course
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission teacher
     *
     *
     * @apiSuccess {Object} course
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .get(teacher(), controller.list);

router
    .route('/:id')
    /**
     * @api {post} api/v1/course Create course
     * @apiDescription Create course
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission teacher
     *
     *
     * @apiSuccess {Object} course
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .get(teacher(), controller.read);

router
    .route('/:id')
    /**
     * @api {post} api/v1/course Create course
     * @apiDescription Create course
     * @apiVersion 1.0.0
     * @apiName Create
     * @apiPermission teacher
     *
     *
     * @apiSuccess {Object} course
     *
     * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
     * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
     */
    .put(teacher(), controller.update);

router
    .route('/:id')
    /**
       * @api {post} api/v1/course Create course
       * @apiDescription Create course
       * @apiVersion 1.0.0
       * @apiName Create
       * @apiPermission teacher
       *
       *
       * @apiSuccess {Object} course
       *
       * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
       * @apiError (Forbidden 403)     Forbidden     Only admin can access the data
       */
    .delete(teacher(), controller.delete);

module.exports = router;
